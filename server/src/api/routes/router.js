import express from 'express'
import { name, version } from '../../../package.json'
import courseRoutesV1 from '../routes/v1/course.routes'
import registrationRoutesV1 from '../routes/v1/registration.routes'

module.exports = app => {
  const router = express.Router()

  router.route('/').get((request, response) => {
    response.send({ name, version })
  })

  courseRoutesV1(router)
  registrationRoutesV1(router)

  app.use('/v1', router)
}
