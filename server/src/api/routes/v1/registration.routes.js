import registrationController from '../../controllers/registration.controller'

//module.exports = router => {
// router.route('/curso').get(cursoController.getAllCurso)

//  router.route('/curso/:idcurso').get(cursoController.getAllCurso)
//}

module.exports = router => {
  router.route('/courses/:course_id/registrations').get(registrationController.index)
  router.route('/courses/:course_id/registrations').post(registrationController.create)
}
