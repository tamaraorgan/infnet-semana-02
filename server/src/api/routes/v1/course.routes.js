import courseController from '../../controllers/course.controller'

//module.exports = router => {
// router.route('/curso').get(cursoController.getAllCurso)

//  router.route('/curso/:idcurso').get(cursoController.getAllCurso)
//}

module.exports = router => {
  router.route('/courses').get(courseController.index)
  router.route('/courses').post(courseController.create)
}
