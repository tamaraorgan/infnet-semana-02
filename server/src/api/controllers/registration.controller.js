const db = require('../../models/index')

module.exports = {
  async index(request, response) {
    const { course_id } = request.params

    const registrations = await db.Course.findByPk(course_id, {
      include: { association: 'registrations' }
    })

    return response.json(registrations)
  },

  async create(request, response) {
    const { course_id } = request.params
    const { id, name, email, birth_date } = request.body

    const registration = await db.Course.findByPk(course_id)

    if (!registration) {
      return response.status(400).json({ error: 'Registration not found' })
    }

    const registrations = await db.Registration.create({
      id,
      name,
      email,
      birth_date,
      course_id
    })

    return response.json(registrations)
  }
}
