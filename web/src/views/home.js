import { useEffect, useState } from "react";
import { getServiceCursos } from "../services/curso.service";

function Home() {
  const [cursos, setCursos] = useState([]);

  useEffect(() => {
    const cursoServicesGet = async () => {
      const dataCursos = await getServiceCursos();
      setCursos(dataCursos.data);
    };
    cursoServicesGet();
  }, []);

  console.log(cursos);
  return (
    <>
      <h1>Home</h1>
      <ul>
        {cursos.map((curso, i) => (
          <li key={i}>{curso.name}</li>
        ))}
      </ul>
    </>
  );
}

export default Home;
